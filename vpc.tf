resource "aws_vpc" "vpc" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"

  tags = {
    Name = "Prod-VPC"
  }

}

resource "aws_internet_gateway" "myigw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "prod_igw"
  }
}

resource "aws_subnet" "natalb" {
  count                   = var.az_count
  cidr_block              = cidrsubnet(aws_vpc.vpc.cidr_block, 8, var.az_count + count.index)
  availability_zone       = slice(data.aws_availability_zones.az.names, 0, 2)[count.index]
  vpc_id                  = aws_vpc.vpc.id
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.subnet1}_${count.index + 1}"
  }
}

resource "aws_subnet" "web" {
  count                   = var.az_count
  cidr_block              = cidrsubnet(aws_vpc.vpc.cidr_block, 5, var.az_count + count.index + 1)
  availability_zone       = element(slice(data.aws_availability_zones.az.names, 0, 2), count.index)
  vpc_id                  = aws_vpc.vpc.id
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.web}_${count.index + 1}"
  }
}

resource "aws_subnet" "app" {
  count                   = var.az_count
  cidr_block              = cidrsubnet(aws_vpc.vpc.cidr_block, 8, var.az_count + count.index +2)
  availability_zone       = element(slice(data.aws_availability_zones.az.names, 0, 2), count.index)
  vpc_id                  = aws_vpc.vpc.id
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.app}_${count.index + 1}"
  }
}

resource "aws_subnet" "db" {
  count                   = var.az_count
  cidr_block              = cidrsubnet(aws_vpc.vpc.cidr_block, 7, var.az_count + count.index +3)
  availability_zone       = element(slice(data.aws_availability_zones.az.names, 0, 2), count.index)
  vpc_id                  = aws_vpc.vpc.id
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.db}_${count.index + 1}"
  }
}

# --------------------------------------------------------------------------------------------------------------
#                     ROUTE TABLE
# --------------------------------------------------------------------------------------------------------------

resource "aws_route_table" "natalb_route_table" {
  count  = var.az_count
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myigw.id
  }
  tags = {
    Name = "${var.nb-rt}_${count.index + 1}"
  }
}
resource "aws_route_table_association" "nb_route-association" {
  count = var.az_count

  subnet_id      = element(aws_subnet.natalb.*.id, count.index)
  route_table_id = element(aws_route_table.natalb_route_table.*.id, count.index)
}

resource "aws_route_table" "web_route_table" {
  count  = var.az_count
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myigw.id
  }
  tags = {
    Name = "${var.web-rt}"
  }
}
resource "aws_route_table_association" "web_route-association" {
  count = var.az_count

  subnet_id      = element(aws_subnet.web.*.id, count.index)
  route_table_id = element(aws_route_table.web_route_table.*.id, count.index)
}

resource "aws_route_table" "app_route_table" {
  count  = var.az_count
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myigw.id
  }
  tags = {
    Name = "${var.app-rt}"
  }
}
resource "aws_route_table_association" "ap_route-association" {
  count = var.az_count

  subnet_id      = element(aws_subnet.app.*.id, count.index)
  route_table_id = element(aws_route_table.app_route_table.*.id, count.index)
}


resource "aws_route_table" "db_route_table" {
  count  = var.az_count
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myigw.id
  }
  tags = {
    Name = "${var.db-rt}"
  }
}
resource "aws_route_table_association" "db_route-association" {
  count = var.az_count

  subnet_id      = element(aws_subnet.db.*.id, count.index)
  route_table_id = element(aws_route_table.db_route_table.*.id, count.index)
}

# Elastic IP configuration
resource "aws_eip" "eip" {
  count = var.az_count

  vpc        = true
  depends_on = [aws_internet_gateway.myigw]
  tags = {
    Name = "eip_${count.index + 1}"
  }
}

# --------------------------------------------------------------------------------------------------------------
#                     NAT GATEWAY
# --------------------------------------------------------------------------------------------------------------

resource "aws_nat_gateway" "nat" {
  count         = var.az_count
  subnet_id     = element(aws_subnet.natalb.*.id, count.index)
  allocation_id = element(aws_eip.eip.*.id, count.index)
  tags = {
    Name = "Nat_Gateway_${count.index + 1}"
  }
}