subnet1 = "Prod-NAT-ALB-Subnet"

web = "Prod-Webserver-Subnet"

app = "Prod-Appserver-Subnet"

db = "Prod-db-Subnet"

nb-rt = "Prod-NAT-ALB-Public-RT"

web-rt = "Prod-Webserver-RT"

app-rt = "Prod-Appserver-RT"

db-rt = "Prod-Database-RT"