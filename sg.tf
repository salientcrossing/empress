resource "aws_security_group" "bastion_host_security_group" {
  name        = "Bastion access"
  description = "Bastion group"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description = "VPC bound"
    from_port   = local.bsh_from
    to_port     = local.bsh_to
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "bastion_grp"
  }
}

resource "aws_security_group" "frontend_lb_security_group" {
  name        = "frontend-lb-security-access"
  description = "Frontend LB Security Group"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description = "HTTP Access"
    from_port   = local.http_from
    to_port     = local.https_to
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Frontend-LB-Security-Group_sg"
  }
}

resource "aws_security_group" "webservers_security_group" {
  name        = "webserver access"
  description = "Webserver group"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port   = local.http_from
    to_port     = local.https_to
    protocol    = "tcp"
    cidr_blocks = [aws_security_group.frontend_lb_security_group.name]
  }


  ingress {

    from_port   = local.ssh_from
    to_port     = local.ssh_to
    protocol    = "tcp"
    cidr_blocks = [aws_security_group.bastion_host_security_group.name]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }


  tags = {
    Name = "Webserver_grp"
  }
}

resource "aws_security_group" "backend_lb_security_group" {
  name        = "Backend-LB-Security-Access"
  description = "Backend LB Security Group"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description = "HTTP Access"
    from_port   = local.http_from
    to_port     = local.http_to
    protocol    = "tcp"
    cidr_blocks = [aws_security_group.webservers_security_group.name]
    
  }

  ingress {
    from_port   = local.https_from
    to_port     = local.https_to
    protocol    = "tcp"
    cidr_blocks = [aws_security_group.webservers_security_group.name]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "backend-lb_sg"
  }
}

resource "aws_security_group" "appservers_security_group" {
  name        = "Appserver access"
  description = "Appserver sec group"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description = "HTTP Access"
    from_port   = local.http_from
    to_port     = local.http_to
    protocol    = "tcp"
    cidr_blocks = [aws_security_group.backend_lb_security_group.name]
  }

  ingress {
    from_port   = local.https_from
    to_port     = local.https_to
    protocol    = "tcp"
    cidr_blocks = [aws_security_group.backend_lb_security_group.name]
  }

  ingress {

    from_port   = local.ssh_from
    to_port     = local.ssh_to
    protocol    = "tcp"
    cidr_blocks = [aws_security_group.bastion_host_security_group.name]
  }

  tags = {
    Name = "Webserver_grp"
  }
}

resource "aws_security_group" "database_security_group" {
  name        = "database access"
  description = "database sec group"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description = "VPC bound"
    from_port   = local.db_from
    to_port     = local.db_to
    protocol    = "tcp"
    cidr_blocks = [aws_security_group.appservers_security_group.name, aws_security_group.bastion_host_security_group.name]
  }

  tags = {
    Name = "Db_sec_grp"
  }
}