variable "subnet1" {
  type = string
}

variable "web" {
  type = string
}

variable "app" {
  type = string
}

variable "db" {
  type = string
}

variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
  default     = "2"
}

variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "nb-rt" {
  type = string
}
variable "web-rt" {
  type = string
}

variable "app-rt" {
  type = string
}

variable "db-rt" {
  type = string
}