# resource "alb" "myalb" {
#   name = ""
#   security_group = ""
#   subnets = []

#   tags ={
#     Name = "Prod-Frontend-LB"
#   }
# }

# # --------------------------------------------------------------------------------------------------------------
# #                     TARGET GROUP
# # --------------------------------------------------------------------------------------------------------------

# resource "aws_alb_target_group" "bckend_grp" {
#   name     = "Backend-LB-HTTP-TG"
#   port     = 80
#   protocol = "HTTP1"
#   vpc_id   = aws_vpc.vpc.id
#   stickiness {
#     type = "lb_cookie"
#   }
#   # Alter the destination of the health check to be the login page.
#   health_check {
#     healthy_threshold = 3
#     unhealthy_threshold = 10
#     timeout = 5
#     interval = 10
#     path = "/VenturaMailingApp.php"
#     port = 80
#   }
# }